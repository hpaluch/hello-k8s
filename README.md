# hello-k8s

Simple Node.js Docker application that will
be later deployed as Pod to Kubernetes.

# Building local docker image

Tested Host OS: Ubuntu 20
- install Docker:
  ```bash
  sudo apt-get install docker.io
  ```
- put yourself to `docker` group and relogin:
  ```bash
  sudo usermod -G docker -a $USER
  ```
- install Node.js and Yarn and curl:
  ```bash
  sudo apt-get install nodejs yarnpkg curl
  ```
- now build docker image using:
  ```bash
  docker build -t hello-k8s .
  ```
- run your new image `hello-k8s`:
  ```bash
  docker run -it -p 8080:8080 hello-k8s
  # it will block this terminal
  ```
- try in another terminal:
  ```bash
  curl -fsSL http://127.0.0.1:8080
  # also watch output on your Docker terminal
  ```
- to terminate `hello-k8s` press `Ctrl`-`c` on your Docker terminal

# Using this Docker image

You can use image from my GitLab Container Registry, for example:
```bash
docker run -it -p 8080:8080 --name hello-k8s-1 registry.gitlab.com/hpaluch/hello-k8s
```

# Kubernetes example

Requirements:
- running Kubernetes cluster (2 Nodes or more recommended)
- I'm using [MicroK8s under LXD](https://microk8s.io/docs/lxd)
  - My Host OS: `Ubuntu 20.04.3 LTS`
  - Hardware: 4 vCPUs, 16GB RAM  (not needed but handy in future)
  - host `lxc --version`: `4.0.7`
  - containers snap versions:
    ```bash
    kubectl get nodes
    NAME     STATUS   ROLES    AGE     VERSION
    k8s-n1   Ready    <none>   4h2m    v1.22.2-3+9ad9ee77396805
    k8s-n2   Ready    <none>   3h17m   v1.22.2-3+9ad9ee77396805
    ```
  

At first you should create Deployment called `hello-k8s-deployment` using:
```bash
kubectl apply -f manifests/hello-k8s-deployment.yaml
```
You can list existing deployments using:
```bash
kubectl get deployment hello-k8s-deployment -o wide

NAME                   READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES                                  SELECTOR
hello-k8s-deployment   3/3     3            3           21s   hello-k8s    registry.gitlab.com/hpaluch/hello-k8s   app=hello-k8s
```
Ensure that READY is `3/3`.

Also check where is our Pod deployed:
```bash
kubectl get pods -l app=hello-k8s -o wide

NAME                                    READY   STATUS    RESTARTS   AGE   IP             NODE     NOMINATED NODE   READINESS GATES
hello-k8s-deployment-58cdffc6c8-sr4wd   1/1     Running   0          67s   10.1.215.68    k8s-n1   <none>           <none>
hello-k8s-deployment-58cdffc6c8-gvhbg   1/1     Running   0          67s   10.1.111.196   k8s-n2   <none>           <none>
hello-k8s-deployment-58cdffc6c8-zb7cp   1/1     Running   0          67s   10.1.111.197   k8s-n2   <none>           <none>
```

Now we have to create `Service` (Ingress can talk to Service only):
```bash
kubectl apply -f manifests/hello-k8s-service.yaml
```

Verify that Service exist:
```bash
kubectl get svc hello-k8s-svc -o wide

NAME            TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE   SELECTOR
hello-k8s-svc   ClusterIP   10.152.183.192   <none>        80/TCP    60s   app=hello-k8s
```

And define Ingress:
- NOTE: Up-to-date definition is on
  - https://github.com/kubernetes/kubernetes/issues/90077#issuecomment-768423075

```bash
kubectl apply -f manifests/hello-k8s-ingress.yaml
```

Now you can try something like:
- `curl http://ANY_NODE_EXTERNAL_IP`
- and should see something like:
  ```
  Hello, World!
  Hostname: hello-k8s-deployment-58cdffc6c8-zb7cp
  ```


# Internal notes

Building and publishing Image to GitLab Container Registry:
```bash
docker login registry.gitlab.com
docker build -t registry.gitlab.com/hpaluch/hello-k8s .
docker push registry.gitlab.com/hpaluch/hello-k8s
```

# Notes

To get Node.js core modules try this command:
```bash
nodejs -e 'console.log(require("module").builtinModules)'
```
Based on:
- https://stackoverflow.com/a/51945595

# Copyright/Resources

The server.js is based on:
- https://github.com/GoogleCloudPlatform/kubernetes-engine-samples/blob/master/hellonode/server.js

Docker tips come from:
- https://stackoverflow.com/a/47228286

