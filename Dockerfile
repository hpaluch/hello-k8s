# Dockerfile for hello-k8s app
# based on: https://stackoverflow.com/a/47228286
FROM node:12-alpine

COPY package.json yarn.lock *.js /app/
RUN cd /app && yarn install --pure-lockfile

WORKDIR /app
ENTRYPOINT ["yarn","run","start"]

